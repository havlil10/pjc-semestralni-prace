#include <vector>
using namespace std;
typedef vector< vector<double> > Matrix;

class matrixHolder  {
public:
    matrixHolder() = default;
    explicit matrixHolder(Matrix matrix);
    Matrix getMatrix();

    void run();

private:
    Matrix getMatrixFromUser();
    void applyGEM();
    vector<double> countSolution();
    vector< vector<double> > getSpanVectors();
    void printSolutionVector(const vector<double>& reversedSolution);
    void printSpanVectors(const Matrix& matrixOfSpans);

    Matrix matrix;
};
